import { Module } from '@nestjs/common';
import { Module1Module } from './module1/module1.module';
import { Module2Module } from './module2/module2.module';
import { IntermediateService } from './intermediate.service';
import { IntermediateController } from './intermediate.controller';

@Module({
  imports: [Module1Module, Module2Module],
  exports: [Module1Module, Module2Module],
  providers: [IntermediateService],
  controllers: [IntermediateController],
})
export class IntermediateModule {
  constructor() {
    console.log('IntermediateModule => constructor');
  }
}
