import { Controller, Get } from '@nestjs/common';
import { GlobalService } from './global.service';

@Controller('global')
export class GlobalController {
  constructor(private globalService: GlobalService) {
    console.log('GlobalController => constructor');
  }

  @Get()
  home(): string {
    return this.globalService.getMessage();
  }
}
