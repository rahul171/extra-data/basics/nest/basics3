import { Injectable } from '@nestjs/common';

@Injectable()
export class GlobalService {
  constructor() {
    console.log('GlobalService => constructor');
  }

  getMessage(): string {
    return 'welcome to the global service';
  }
}
