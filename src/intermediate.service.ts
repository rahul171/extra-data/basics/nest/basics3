import { Injectable } from '@nestjs/common';

@Injectable()
export class IntermediateService {
  constructor() {
    console.log('IntermediateService => constructor');
  }

  getMessage(): string {
    return 'welcome to the intermediate service';
  }
}
