import { Module } from '@nestjs/common';
import { Module1Controller } from './module1.controller';
import { Module1Service } from './module1.service';
import { GlobalModule } from '../global/global.module';

@Module({
  controllers: [Module1Controller],
  providers: [Module1Service],
  exports: [Module1Service],
})
export class Module1Module {
  constructor() {
    console.log('Module1Module => constructor');
  }
}
