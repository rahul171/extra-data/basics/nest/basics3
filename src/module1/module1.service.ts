import { Injectable } from '@nestjs/common';
import { GlobalService } from '../global/global.service';

@Injectable()
export class Module1Service {
  private values: number[] = [];

  constructor(private globalService: GlobalService) {
    console.log('Module1Service => constructor');
  }

  getValues(): number[] {
    return this.values;
  }

  addValue(value: number): number {
    this.values.push(value);
    return value;
  }
}
