import { Controller, Get, Param } from '@nestjs/common';
import { Module1Service } from './module1.service';
import { GlobalService } from '../global/global.service';

@Controller('module1')
export class Module1Controller {
  constructor(
    private module1Service: Module1Service,
    private globalService: GlobalService,
  ) {
    console.log('Module1Controller => constructor');
  }

  @Get()
  getValues(): number[] {
    return this.module1Service.getValues();
  }

  @Get('add/:value')
  addValue(@Param('value') value): number[] {
    value = parseInt(value);
    this.module1Service.addValue(value);
    return this.module1Service.getValues();
  }

  @Get('global')
  getGlobalMessage(): string {
    return this.globalService.getMessage();
  }
}
