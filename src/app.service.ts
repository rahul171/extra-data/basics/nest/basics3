import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  constructor() {
    console.log('AppService => constructor');
  }

  getMessage(): string {
    return 'welcome to the app service';
  }
}
