import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { IntermediateModule } from './intermediate.module';
import { GlobalModule } from './global/global.module';

@Module({
  imports: [IntermediateModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor() {
    console.log('AppModule => constructor');
  }
}
