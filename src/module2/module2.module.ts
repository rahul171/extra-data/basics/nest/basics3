import { Module } from '@nestjs/common';
import { Module2Controller } from './module2.controller';
import { Module2Service } from './module2.service';
import { GlobalModule } from '../global/global.module';

@Module({
  imports: [GlobalModule],
  controllers: [Module2Controller],
  providers: [Module2Service],
})
export class Module2Module {
  constructor(private module2Service: Module2Service) {
    console.log(
      'Module2Module => constructor =>',
      this.module2Service.getValues(),
    );
  }

  // constructor() {
  //   console.log('Module2Module => constructor');
  // }
}
