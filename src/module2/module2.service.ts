import { Injectable } from '@nestjs/common';

@Injectable()
export class Module2Service {
  private values: number[] = [];

  constructor() {
    console.log('Module2Service => constructor');
  }

  getValues(): number[] {
    return this.values;
  }

  addValue(value: number): number {
    this.values.push(value);
    return value;
  }
}
