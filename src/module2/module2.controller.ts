import { Controller, Get, Param } from '@nestjs/common';
import { Module2Service } from './module2.service';
import { GlobalService } from '../global/global.service';

@Controller('module2')
export class Module2Controller {
  constructor(
    private module2Service: Module2Service,
    private globalService: GlobalService,
  ) {
    console.log('Module2Controller => constructor');
  }

  @Get()
  getValues(): number[] {
    return this.module2Service.getValues();
  }

  @Get('add/:value')
  addValue(@Param('value') value): number[] {
    value = parseInt(value);
    this.module2Service.addValue(value);
    return this.module2Service.getValues();
  }

  @Get('global')
  getGlobalMessage(): string {
    return this.globalService.getMessage();
  }
}
