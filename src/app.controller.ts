import { Controller, Get, Param } from '@nestjs/common';
import { AppService } from './app.service';
import { Module1Service } from './module1/module1.service';
import { GlobalService } from './global/global.service';

@Controller()
export class AppController {
  constructor(
    private appService: AppService,
    private module1Service: Module1Service,
    private globalService: GlobalService,
  ) {
    console.log('AppController => constructor');
  }

  @Get()
  home(): string {
    return this.appService.getMessage();
  }

  @Get('values')
  getValues(): number[] {
    return this.module1Service.getValues();
  }

  @Get('values/add/:value')
  addValue(@Param('value') value): number[] {
    value = parseInt(value);
    this.module1Service.addValue(value);
    return this.module1Service.getValues();
  }

  @Get('getglobal')
  getGlobalMessage(): string {
    return this.globalService.getMessage();
  }
}
